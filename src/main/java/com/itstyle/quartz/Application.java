package com.itstyle.quartz;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 启动类
 * 创建者 科帮网
 * 创建时间	2018年3月28日
 * API接口测试：http://localhost:8080/quartz/swagger-ui.html
 * Spring Loaded
 *
 *<build>
 *     <plugins>
 *         <plugin>
 *             <groupId>org.springframework.boot</groupId>
 *             <artifactId>spring-boot-maven-plugin</artifactId>
 *             <dependencies>
 *                 <dependency>
 *                     <groupId>org.springframework</groupId>
 *                     <artifactId>springloaded</artifactId>
 *                     <version>1.2.5.RELEASE</version>
 *                 </dependency>
 *             </dependencies>
 *         </plugin>
 *     </plugins>
 * </build>
 * 这种方式是以Maven插件的形式去加载，所以启动时使用通过Maven命令mvn spring-boot:run启动，而通过Application.run方式启动的会无效，因为通过应用程序启动时，已经绕开了Maven插件机制。
 *
 * 用application.run启动时没有任务乱码，而用maven启动则有乱码，搞不懂,
 */
@SpringBootApplication
public class Application {
	private static final Logger logger = Logger.getLogger(Application.class);
	
	public static void main(String[] args) throws InterruptedException, SchedulerException {
		SpringApplication.run(Application.class, args);
		logger.info("项目启动 ");
	}
}