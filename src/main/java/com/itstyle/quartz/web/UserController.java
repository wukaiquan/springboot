package com.itstyle.quartz.web;

import com.alibaba.fastjson.JSONObject;
import com.itstyle.quartz.entity.Result;
import com.itstyle.quartz.entity.UserEntity;
import com.itstyle.quartz.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

/**@Controller和@RestController的区别？
 *   知识点：@RestController注解相当于@ResponseBody ＋ @Controller合在一起的作用。
 *   1) 如果只是使用@RestController注解Controller，则Controller中的方法无法返回jsp页面，或者html，
 *   配置的视图解析器 InternalResourceViewResolver不起作用，返回的内容就是Return 里的内容。
 *   2) 如果需要返回到指定页面，则需要用 @Controller配合视图解析器InternalResourceViewResolver才行。
 *     如果需要返回JSON，XML或自定义mediaType内容到页面，则需要在对应的方法上加上@ResponseBody注解。
 *  所以才有IndexController返回界面,indexController中有2中方式放回界面，一种是根目录，一种是二级目录.
 *
 *  1.使用@Controller 注解，在对应的方法上，视图解析器可以解析return 的jsp,html页面，并且跳转到相应页面
 *  若返回json等内容到页面，则需要加@ResponseBody注解
 *
 *   2.@RestController注解，相当于@Controller+@ResponseBody两个注解的结合，
 *   返回json数据不需要在方法前面加@ResponseBody注解了，但使用@RestController这个注解，就不能返回jsp,html页面，
 *   视图解析器无法解析jsp,html页面
 * */
@Api(tags = "userContro")
@RestController
@RequestMapping("/user")
public class UserController {
    private final static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userServiceImp;
    @Autowired
    private HttpServletRequest request;

    @ApiOperation(value="user列表")
    @RequestMapping("/userlist")
    public Result userlist(UserEntity userEntity, Integer pageNo, Integer pageSize){
        LOGGER.info("userlist");
        List<UserEntity> list = userServiceImp.selectUserlist(userEntity, pageNo, pageSize);
        return Result.ok(list);
    }



    @ApiOperation(value="user列表")
    @RequestMapping("/userlist2")
    public String userlist2(){
        LOGGER.info("userlist");
        List<UserEntity> list = userServiceImp.selectUserlist2();
        return JSONObject.toJSONString(list);
    }


    @ApiOperation(value = "douseredit")
    @RequestMapping("/selectUser")
    public Result selectUser(String uid){
        userServiceImp.selectUserEntityByuid(uid);
        return Result.ok();
    }

    /**1.界面的userEdit.shtml链接和RequestMapping的链接不能冲突,http://localhost:8080/quartz/user/douseredit  400 ?服务器没这个链接,难道一定要用PostMapping?,我记得RequestMapping
     * 可以post和get请求都可以的。4.0版本的spring,难道spring-boot不行？,ajax data参数没写，为什么别人没写就进行，我没写就不行？,user对象为空,datatype:json,
     * 不论是postMapping还是RequestMapping，这个user都没有值,user 多出了2个属性_index和，_rowKey,别人写的能接收到，我的不能，晕，还是用参数接收算了。反正正式的尽量不相信前台传来的数据*/
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @ApiOperation(value = "douseredit")
    @RequestMapping("/douseredit")
    public Result userEdit(String uid,BigDecimal qbao){
        //String user1 = request.getParameter("user");//user 多出了2个属性_index和，_rowKey
        //UserEntity user = (UserEntity)JSONObject.parse(user1);
        UserEntity user = new UserEntity();
        LOGGER.info("userEdit"+user.getUid()+user.getMobile()+user.getAssetbalance());
        if(qbao.compareTo(new BigDecimal(0)) <0){
            return Result.error(1,"Q宝不能小于0");
        }
        userServiceImp.editUserByid(uid,qbao);
        return Result.ok();
    }
    
}
