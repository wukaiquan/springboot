package com.itstyle.quartz.service;

import com.itstyle.quartz.entity.Result;
import com.itstyle.quartz.entity.UserEntity;

import java.math.BigDecimal;
import java.util.List;

public interface UserService {
    /**查询用户记录*/
    List<UserEntity> selectUserlist(UserEntity userEntity, Integer pageNo, Integer pageSize);
    /**修改用户信息*/
    Result editUserByid(String uid, BigDecimal qbao);

    /**查询客户信息*/
    UserEntity selectUserEntityByuid(String uid);

    List<UserEntity> selectUserlist2();
}
