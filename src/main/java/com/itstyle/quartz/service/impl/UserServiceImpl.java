package com.itstyle.quartz.service.impl;

import com.itstyle.quartz.dynamicquery.DynamicQuery;
import com.itstyle.quartz.entity.Result;
import com.itstyle.quartz.entity.UserEntity;
import com.itstyle.quartz.service.UserService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private DynamicQuery dynamicQuery;

    @Override
    public List<UserEntity> selectUserlist(UserEntity quartz, Integer pageNo, Integer pageSize) {
        StringBuffer nativeSql = new StringBuffer();
        nativeSql.append("SELECT   " +
                "u.userid as userid, u.uid as uid, u.tuijian as tuijian, u.tuijiannum as tuijiannum, " +
                "u.assetbalance as assetbalance, u.freezeassetbalance as freezeassetbalance, " +
                "u.reassetbalance as reassetbalance,u.username as username,u.mobile as mobile," +
                "u.score as score , u.tdnum as tdnum ,u.qbao as qbao    ");
        nativeSql.append(" from t_front_user as  u ");
        Object[] params = new  Object[]{};
        List<UserEntity> list =dynamicQuery.nativeQueryListModel(UserEntity.class, nativeSql.toString(),params);
        System.out.println("---------user.size()----------"+list.size());
        return list;
    }

    @Override
    public UserEntity selectUserEntityByuid(String uid) {
        StringBuffer nativeSql = new StringBuffer();
        nativeSql.append("SELECT   " +
                "u.userid as userid, u.uid as uid, u.tuijian as tuijian, u.tuijiannum as tuijiannum, " +
                "u.assetbalance as assetbalance, u.freezeassetbalance as freezeassetbalance, " +
                "u.reassetbalance as reassetbalance,u.username as username,u.mobile as mobile," +
                "u.score as score , u.tdnum as tdnum ,u.qbao as qbao    ");
        nativeSql.append(" from t_front_user as  u where u.uid= "+uid);
        Object[] params = new  Object[]{};
        UserEntity user = (UserEntity)dynamicQuery.nativeQueryEntity(UserEntity.class, nativeSql.toString(), params);
        return user;
    }

    @Override
    public List<UserEntity> selectUserlist2() {
        StringBuffer nativeSql = new StringBuffer();
        nativeSql.append("SELECT   " +
                "u.userid as userid, u.uid as uid, u.tuijian as tuijian, u.tuijiannum as tuijiannum, " +
                "u.assetbalance as assetbalance, u.freezeassetbalance as freezeassetbalance, " +
                "u.reassetbalance as reassetbalance,u.username as username,u.mobile as mobile," +
                "u.score as score , u.tdnum as tdnum ,u.qbao as qbao    ");
        nativeSql.append(" from t_front_user as  u ");
        Object[] params = new  Object[]{};
        List<UserEntity> list =dynamicQuery.nativeQueryListModel(UserEntity.class, nativeSql.toString(),params);
        return list;
    }

    @Transactional
    public Result editUserByid(String uid, BigDecimal qbao) {
        StringBuffer nativeSql = new StringBuffer();
        nativeSql.append("update   " +
                "t_front_user u set u.qbao = " + qbao);
        nativeSql.append(" where u.uid= "+uid);
        Object[] params = new  Object[]{};
        int result = (Integer) dynamicQuery.updateQueryEntity(UserEntity.class, nativeSql.toString(), params);
        if(result == 0){
            return Result.error(1,"更新错误");
        }
        return Result.ok();


    }

}
