package com.itstyle.quartz.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class UserEntity {

    //用户id
    private Integer userid;
    //UID
    private String uid;
    //用户名
    private String username;
    //真实姓名
    private String truename;
    //密码
    private String password;
    //支付密码
    private String paypassword;
    //手机
    private String mobile;
    //钱包地址
    private String address;
    //推荐你的人
    private String tuijian;
    //你推荐的人数
    private Integer tuijiannum;
    //资产余额
    private BigDecimal assetbalance;
    //冻结资产
    private BigDecimal freezeassetbalance;
    //用户的复投券数量
    private BigDecimal reassetbalance;
    //积分
    private BigDecimal score;
    //td币数量
    private BigDecimal tdnum;
    //冻结td币数量
    private BigDecimal freezetdnum;
    //释放量
    private BigDecimal shifang;
    //email
    private String email;
    //用户状态
    private String status;
    //流通奖和见点奖会员等级(0：非会员，1：一星会员，2：二星会员，3：三星会员，4：四星会员）
    private String grade;
    //注册时间
    private Date regtime;
    //更新时间
    private Date updatetime;
    //登陆时间
    private Date logintime;
    //登录ip
    private String loginip;
    //保证金
    private BigDecimal cashdeposit;
    //比特币数量
    private BigDecimal btcnum;
    //比特币冻结数量
    private BigDecimal freezebtcnum;
    //莱特币数量
    private BigDecimal ltcnum;
    //莱特币冻结数量
    private BigDecimal freezeltcnum;
    //以太坊数量
    private BigDecimal ethnum;
    //以太坊冻结数量
    private BigDecimal freezeethnum;



    //搜索的开始时间
    private String startTime;
    //搜索的结束时间
    private String endTime;

    //第几代父级
    private Integer daishu;
//    //分享奖比例
//    private BigDecimal fenxiangrate;
//    //流通奖比例
//    private BigDecimal liutongrate;
//    //见点奖比例
//    private BigDecimal jiandianrate;

    //店铺待结算收益
    private BigDecimal frezzstoreincome;
    //店铺总收益
    private BigDecimal storeincome;


    //Q宝数量
    private BigDecimal qbao;
    //冻结的Q宝数量
    private BigDecimal frezzeqbao;
    //Q宝等级
    private String qbaograde;//qbao等级
    //Q宝日息
    private BigDecimal qbaoscore;


    //cflb数量
    private BigDecimal cflbnum;


    public UserEntity(){super();}

    public UserEntity(Integer userid, String uid, String username, String truename, String password, String paypassword, String mobile, String address, String tuijian, Integer tuijiannum, BigDecimal assetbalance, BigDecimal freezeassetbalance, BigDecimal reassetbalance, BigDecimal score, BigDecimal tdnum, BigDecimal freezetdnum, BigDecimal shifang, String email, String status, String grade, Date regtime, Date updatetime, Date logintime, String loginip, BigDecimal cashdeposit, BigDecimal btcnum, BigDecimal freezebtcnum, BigDecimal ltcnum, BigDecimal freezeltcnum, BigDecimal ethnum, BigDecimal freezeethnum, String startTime, String endTime, Integer daishu, BigDecimal fenxiangrate, BigDecimal liutongrate, BigDecimal jiandianrate, BigDecimal frezzstoreincome, BigDecimal storeincome, BigDecimal qbao, BigDecimal frezzeqbao, String qbaograde, BigDecimal qbaoscore, int qbaoRecordType, List<String> qbaoRecordStatus, BigDecimal cflbnum) {
        this.userid = userid;
        this.uid = uid;
        this.username = username;
        this.truename = truename;
        this.password = password;
        this.paypassword = paypassword;
        this.mobile = mobile;
        this.address = address;
        this.tuijian = tuijian;
        this.tuijiannum = tuijiannum;
        this.assetbalance = assetbalance;
        this.freezeassetbalance = freezeassetbalance;
        this.reassetbalance = reassetbalance;
        this.score = score;
        this.tdnum = tdnum;
        this.freezetdnum = freezetdnum;
        this.shifang = shifang;
        this.email = email;
        this.status = status;
        this.grade = grade;
        this.regtime = regtime;
        this.updatetime = updatetime;
        this.logintime = logintime;
        this.loginip = loginip;
        this.cashdeposit = cashdeposit;
        this.btcnum = btcnum;
        this.freezebtcnum = freezebtcnum;
        this.ltcnum = ltcnum;
        this.freezeltcnum = freezeltcnum;
        this.ethnum = ethnum;
        this.freezeethnum = freezeethnum;
        this.startTime = startTime;
        this.endTime = endTime;
        this.daishu = daishu;
        this.frezzstoreincome = frezzstoreincome;
        this.storeincome = storeincome;
        this.qbao = qbao;
        this.frezzeqbao = frezzeqbao;
        this.qbaograde = qbaograde;
        this.qbaoscore = qbaoscore;

        this.cflbnum = cflbnum;
    }




    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPaypassword() {
        return paypassword;
    }

    public void setPaypassword(String paypassword) {
        this.paypassword = paypassword;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTuijian() {
        return tuijian;
    }

    public void setTuijian(String tuijian) {
        this.tuijian = tuijian;
    }

    public Integer getTuijiannum() {
        return tuijiannum;
    }

    public void setTuijiannum(Integer tuijiannum) {
        this.tuijiannum = tuijiannum;
    }

    public BigDecimal getAssetbalance() {
        return assetbalance;
    }

    public void setAssetbalance(BigDecimal assetbalance) {
        this.assetbalance = assetbalance;
    }

    public BigDecimal getFreezeassetbalance() {
        return freezeassetbalance;
    }

    public void setFreezeassetbalance(BigDecimal freezeassetbalance) {
        this.freezeassetbalance = freezeassetbalance;
    }

    public BigDecimal getReassetbalance() {
        return reassetbalance;
    }

    public void setReassetbalance(BigDecimal reassetbalance) {
        this.reassetbalance = reassetbalance;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public BigDecimal getTdnum() {
        return tdnum;
    }

    public void setTdnum(BigDecimal tdnum) {
        this.tdnum = tdnum;
    }

    public BigDecimal getFreezetdnum() {
        return freezetdnum;
    }

    public void setFreezetdnum(BigDecimal freezetdnum) {
        this.freezetdnum = freezetdnum;
    }

    public BigDecimal getShifang() {
        return shifang;
    }

    public void setShifang(BigDecimal shifang) {
        this.shifang = shifang;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Date getRegtime() {
        return regtime;
    }

    public void setRegtime(Date regtime) {
        this.regtime = regtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Date getLogintime() {
        return logintime;
    }

    public void setLogintime(Date logintime) {
        this.logintime = logintime;
    }

    public String getLoginip() {
        return loginip;
    }

    public void setLoginip(String loginip) {
        this.loginip = loginip;
    }

    public BigDecimal getCashdeposit() {
        return cashdeposit;
    }

    public void setCashdeposit(BigDecimal cashdeposit) {
        this.cashdeposit = cashdeposit;
    }

    public BigDecimal getBtcnum() {
        return btcnum;
    }

    public void setBtcnum(BigDecimal btcnum) {
        this.btcnum = btcnum;
    }

    public BigDecimal getFreezebtcnum() {
        return freezebtcnum;
    }

    public void setFreezebtcnum(BigDecimal freezebtcnum) {
        this.freezebtcnum = freezebtcnum;
    }

    public BigDecimal getLtcnum() {
        return ltcnum;
    }

    public void setLtcnum(BigDecimal ltcnum) {
        this.ltcnum = ltcnum;
    }

    public BigDecimal getFreezeltcnum() {
        return freezeltcnum;
    }

    public void setFreezeltcnum(BigDecimal freezeltcnum) {
        this.freezeltcnum = freezeltcnum;
    }

    public BigDecimal getEthnum() {
        return ethnum;
    }

    public void setEthnum(BigDecimal ethnum) {
        this.ethnum = ethnum;
    }

    public BigDecimal getFreezeethnum() {
        return freezeethnum;
    }

    public void setFreezeethnum(BigDecimal freezeethnum) {
        this.freezeethnum = freezeethnum;
    }



    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getDaishu() {
        return daishu;
    }

    public void setDaishu(Integer daishu) {
        this.daishu = daishu;
    }



    public BigDecimal getFrezzstoreincome() {
        return frezzstoreincome;
    }

    public void setFrezzstoreincome(BigDecimal frezzstoreincome) {
        this.frezzstoreincome = frezzstoreincome;
    }

    public BigDecimal getStoreincome() {
        return storeincome;
    }

    public void setStoreincome(BigDecimal storeincome) {
        this.storeincome = storeincome;
    }

    public BigDecimal getQbao() {
        return qbao;
    }

    public void setQbao(BigDecimal qbao) {
        this.qbao = qbao;
    }

    public BigDecimal getFrezzeqbao() {
        return frezzeqbao;
    }

    public void setFrezzeqbao(BigDecimal frezzeqbao) {
        this.frezzeqbao = frezzeqbao;
    }

    public String getQbaograde() {
        return qbaograde;
    }

    public void setQbaograde(String qbaograde) {
        this.qbaograde = qbaograde;
    }

    public BigDecimal getQbaoscore() {
        return qbaoscore;
    }

    public void setQbaoscore(BigDecimal qbaoscore) {
        this.qbaoscore = qbaoscore;
    }


    public BigDecimal getCflbnum() {
        return cflbnum;
    }

    public void setCflbnum(BigDecimal cflbnum) {
        this.cflbnum = cflbnum;
    }
}
